class ProductDAO {
  String key;
  String image;
  String name;
  String model;
  String description;

  ProductDAO ({this.key, this.image, this.name, this.model, this.description});
  Map<String, dynamic> toMap(){
    return {
      'key'        : key,
      'image'      : image,
      'name'       : name,
      'model'      : model,
      'description': description,
    };
  } 
}