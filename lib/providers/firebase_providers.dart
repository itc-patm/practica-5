import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:practica_5/models/product_dao.dart';

class FirebaseProvider {
  FirebaseFirestore _firestore;

  CollectionReference _productsCollerction;

  FirebaseProvider() {
    _firestore=FirebaseFirestore.instance;
    _productsCollerction= _firestore.collection('products');
  }

  Stream<QuerySnapshot> getAllProducts() {
    return _productsCollerction.snapshots();
  }

  Future <void> saveProduct(ProductDAO product) {
    return _productsCollerction.add(product.toMap());
  }

  Future<void> updateProduct(ProductDAO product, String documentID) {
    return _productsCollerction.doc(documentID).update(product.toMap());
  }

  Future <void> removeProduct(String documentID) {
    return _productsCollerction.doc(documentID).delete();
  }
  
}