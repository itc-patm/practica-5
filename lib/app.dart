import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:practica_5/screens/create_products_screen.dart';
import 'package:practica_5/screens/list_products_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          debugPrint("${snapshot.error}");
          return MaterialApp(home: Text("Error"));
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            home: ListProducts(),
            routes: {
              '/home'   : (BuildContext context) => ListProducts(),
              '/create' : (BuildContext context) => CreateProduct(),
            },
          );
        }

        return MaterialApp(home: Text("Cargando . . ."));
      },
    );
  }
}