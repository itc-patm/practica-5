import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:practica_5/providers/firebase_providers.dart';
import 'package:practica_5/views/card_product.dart';

class ListProducts extends StatefulWidget {
  ListProducts({Key key}) : super(key: key);

  @override
  _ListProductsState createState() => _ListProductsState();
}

class _ListProductsState extends State<ListProducts> {

  FirebaseProvider firestore;
  FirebaseStorage storage;

  @override
  void initState() {
    super.initState();
    firestore = FirebaseProvider();
    storage = FirebaseStorage.instanceFor(bucket: 'gs://patm-9c5a0.appspot.com');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Products"),
        actions: <Widget>[
          MaterialButton(
              child: Icon(Icons.add, color: Colors.white),
              onPressed: () {
                Navigator.pushNamed(context, "/create");
              })
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: firestore.getAllProducts(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData)
            return Center(child:  CircularProgressIndicator());
          else {
            return ListView(
              children: snapshot.data.docs.map((DocumentSnapshot document) {
                return CardProduct(productDocument: document);
              }).toList(),
            );
          }
        },
      ),
    );

  }
}