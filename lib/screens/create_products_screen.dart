import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:practica_5/models/product_dao.dart';
import 'package:practica_5/providers/firebase_providers.dart';

class CreateProduct extends StatefulWidget {
  CreateProduct({Key key}) : super(key: key);

  @override
  _CreateProductState createState() => _CreateProductState();
}

class _CreateProductState extends State<CreateProduct> {

  FirebaseProvider firebase;
  FirebaseStorage storage;
  


  TextEditingController tcKey = TextEditingController();
  TextEditingController tcName = TextEditingController();
  TextEditingController tcDescription = TextEditingController();
  TextEditingController tcModel = TextEditingController();

  final picker = ImagePicker();
  String imagePath = "" ;

  @override
  void initState(){
    super.initState();

    firebase = FirebaseProvider();
    storage = FirebaseStorage.instanceFor(bucket: 'gs://patm-9c5a0.appspot.com');
  }

  @override
  Widget build(BuildContext context) {

    final imgPerfil = imagePath == ""?
    CircleAvatar(
      backgroundImage: NetworkImage('https://encrypted-tbn2.gstatic.com/shopping?q=tbn:ANd9GcQ0S6Y1aGF7b2teKR6gIJgoC6Bvr56KzyyYDEHJZ_ZRtNcI7T_uP4OAdr5EwWP0j1oRvNYkJdQ&usqp=CAE'),
      radius: 80,
    )
    : CircleAvatar(
      radius: 80,
      child: ClipOval(
        child: Image.file(
          File(imagePath),
          width: 150,
          height: 150,
          fit: BoxFit.cover,
        ),
      )
    );

    final button = RawMaterialButton(
      child: Icon(Icons.image_search, color: Colors.white),
      onPressed: () async {
        final pickedFile = await picker.getImage(source: ImageSource.gallery);
        imagePath = pickedFile != null ? pickedFile.path : "";
        setState(() {});
      },
      elevation: 2.0,
      fillColor: Colors.blue,
      padding: EdgeInsets.all(10.0),
      shape: CircleBorder(),
    ); 

    final txtKey = TextFormField(
      controller: tcKey,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: 'Product Key',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtName = TextFormField(
      controller: tcName,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Name',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtDescription = TextFormField(
      controller: tcDescription,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Description',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    final txtModel = TextFormField(
      controller: tcModel,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Model',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Add Product"),
        actions: <Widget>[
          MaterialButton(
            child: Icon(Icons.save, color: Colors.white),
            onPressed: () async {
              if (imagePath != null) {
                try {
                  String url = "";
                  File img = File(imagePath);
                  Reference ref = storage.ref('images/${tcKey.text}.png');
                  ref.putFile(img).whenComplete(() async {
                    var dowurl = await ref.getDownloadURL();
                    url = dowurl.toString();

                    final product = ProductDAO(
                      key: tcKey.text,
                      image: url,
                      name: tcName.text,
                      model: tcModel.text,
                      description: tcDescription.text,
                    );
                    firebase.saveProduct(product);

                    Navigator.pop(context);
                  });
                  return url; 
                } on FirebaseException catch (e) {
                  print(e);
                }
              }
            }
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Card(
              color: Colors.white10,
              margin: EdgeInsets.all(0.0),
              elevation: 0.0,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    imgPerfil,
                    SizedBox(height: 10,),
                    button,
                    SizedBox(height: 10,),
                    txtKey,
                    SizedBox(height: 10,),
                    txtName,
                    SizedBox(height: 10,),
                    txtModel,
                    SizedBox(height: 10,),
                    txtDescription,
                    SizedBox(height: 10,),
                    //btnSave
                  ],
                ),
              ),
            ),
        ]
      )
    );
  }
}
